public enum Modulation {
    AM(0.1485, 26.1),
    FM(87.5, 108.0);

    private double minFrequency; // MHz
    private double maxFrequency; // MHz

    Modulation(double minFrequency, double maxFrequency) {
        this.minFrequency = minFrequency;
        this.maxFrequency = maxFrequency;
    }

    public double getMinFrequency() {
        return minFrequency;
    }

    public double getMaxFrequency() {
        return maxFrequency;
    }
}
