public class Radio {
    private boolean isOn;
    private int volume;
    private double frequency; // MHz
    private Modulation modulation;

    public Radio(Modulation modulation) {
        this.modulation = modulation;
        this.frequency = modulation.getMinFrequency();
        this.isOn = false;
        this.volume = 0;
    }

    public void turnOn() {
        isOn = true;
    }

    public void turnOff() {
        isOn = false;
    }

    public void volumeUp() {
        changeVolume(5);
    }

    private void changeVolume(int delta) {
        if (isOn) {
            volume = Math.max(Math.min(volume + delta, 100), 0);
        }
    }

    public void volumeDown() {
        changeVolume(-5);
    }

    public void setFrequency(double frequency) {
        if (isOn && frequency >= modulation.getMinFrequency() && frequency <= modulation.getMaxFrequency()) {
            this.frequency = frequency;
        }
    }

    public void setModulation(Modulation modulation) {
        if (isOn && this.modulation != modulation) {
            this.modulation = modulation;
            this.frequency = modulation.getMinFrequency();
        }
    }

    public String toString() {
        return String.format("Radio is %s mit Lautstärke %d, der Modulation %s und der Frequenz %.4f MHz.",
                isOn ? "an" : "aus", volume, modulation, frequency);
    }

}
