import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("Tests for class Radio")
public class RadioTest {


    private Radio initializeRadio(boolean isOn) {
        return initializeRadio(isOn, 0, Modulation.AM);
    }

    private Radio initializeRadio(boolean isOn, int volume, Modulation modulation) {
        Radio radio = new Radio(modulation);
        radio.turnOn();
        int nrOfVolumeUps = Math.round(volume / 5.0f);
        for (int i = 0; i < nrOfVolumeUps; i++) {
            radio.volumeUp();
        }
        if (!isOn) {
            radio.turnOff();
        }
        return radio;
    }

    private void checkRadio(Radio radio, boolean isOnExpected, int volumeExpected,
                            Modulation modulationExpected, double frequencyExpected) {
        String actualSettings = radio.toString();
        String expectedSettings = String.format(
                "Radio is %s mit Lautstärke %d, der Modulation %s und der Frequenz %.4f MHz.",
                isOnExpected ? "an" : "aus", volumeExpected, modulationExpected, frequencyExpected);
        Assertions.assertEquals(expectedSettings, actualSettings);
    }

    @Nested
    @DisplayName("Tests for methods volumeUp and volumeDown")
    class VolumeUpAndDownTest {

        @Test
        public void settingVolumeOnlyPossibleIfRadioIsOff() {
            // Given
            Radio radio = initializeRadio(false);
            // When
            radio.volumeUp();
            // Then
            checkRadio(radio, false, 0, Modulation.AM, Modulation.AM.getMinFrequency());
        }

        @Test
        public void volumeUpThreeTimesAndVolumeDownOneTimeWorksFine() {
            // Given
            Radio radio = initializeRadio(true);
            // When
            radio.volumeUp();
            radio.volumeUp();
            radio.volumeUp();
            radio.volumeDown();
            checkRadio(radio, true, 10, Modulation.AM, Modulation.AM.getMinFrequency());
        }

        @Test
        public void notPossibleToSetVolumeBelowZero() {
            // Given
            Radio radio = initializeRadio(true);
            // When
            radio.volumeDown();
            // Then
            checkRadio(radio, true, 0, Modulation.AM, Modulation.AM.getMinFrequency());
        }

        @Test
        public void notPossibleToSetVolumeAbove100() {
            // Given
            Radio radio = initializeRadio(true, 100, Modulation.AM);
            // When
            radio.volumeUp();
            // Then
            checkRadio(radio, true, 100, Modulation.AM, Modulation.AM.getMinFrequency());
        }
    }

    @Nested
    @DisplayName("Tests for method setFrequency")
    class SetFrequencyTest {

        @Test
        public void settingFrequencyNotPossibleIfRadioIsOff() {
            // Given
            Radio radio = initializeRadio(false, 0, Modulation.AM);
            // When
            radio.setFrequency(Modulation.AM.getMaxFrequency());
            // Then
            checkRadio(radio, false, 0, Modulation.AM, Modulation.AM.getMinFrequency());
        }

        @Test
        public void settingAllowedAMFrequency() {
            // Given
            Radio radio = initializeRadio(true, 0, Modulation.AM);
            // When
            radio.setFrequency(Modulation.AM.getMaxFrequency());
            // Then
            checkRadio(radio, true, 0, Modulation.AM, Modulation.AM.getMaxFrequency());
        }

        @Test
        public void settingAllowedFMFrequency() {
            // Given
            Radio radio = initializeRadio(true, 0, Modulation.FM);
            // When
            radio.setFrequency(Modulation.FM.getMaxFrequency());
            // Then
            checkRadio(radio, true, 0, Modulation.FM, Modulation.FM.getMaxFrequency());
        }

        @Test
        public void settingNotAllowedAMFrequency() {
            // Given
            Radio radio = initializeRadio(true, 0, Modulation.AM);
            // When
            radio.setFrequency(Modulation.AM.getMaxFrequency() + 1.0);
            // Then
            checkRadio(radio, true, 0, Modulation.AM, Modulation.AM.getMinFrequency());
        }

        @Test
        public void settingNotAllowedFMFrequency() {
            // Given
            Radio radio = initializeRadio(true, 0, Modulation.FM);
            // When
            radio.setFrequency(Modulation.FM.getMaxFrequency() + 1.0);
            // Then
            checkRadio(radio, true, 0, Modulation.FM, Modulation.FM.getMinFrequency());
        }
    }

    @Nested
    @DisplayName("Tests for method setModulation")
    class SetModulationTest {

        @Test
        public void settingModulationNotPossibleIfRadioIsOff() {
            // Given
            Radio radio = initializeRadio(false, 0, Modulation.AM);
            // When
            radio.setModulation(Modulation.FM);
            // Then
            checkRadio(radio, false, 0, Modulation.AM, Modulation.AM.getMinFrequency());
        }

        @Test
        public void switchingFromAmToFM() {
            // Given
            Radio radio = initializeRadio(true, 0, Modulation.AM);
            // When
            radio.setModulation(Modulation.FM);
            // Then
            checkRadio(radio, true, 0, Modulation.FM, Modulation.FM.getMinFrequency());
        }

        @Test
        public void switchingFromFmToAM() {
            // Given
            Radio radio = initializeRadio(true, 0, Modulation.FM);
            // When
            radio.setModulation(Modulation.AM);
            // Then
            checkRadio(radio, true, 0, Modulation.AM, Modulation.AM.getMinFrequency());
        }
    }

}
